#define write(fd, string, len) \
  ({ long ret; \
   asm volatile("syscall": "=a"(ret): "a"(1), "D"(fd), "S"(string), "d"(len) : "rcx", "r11", "cc"); \
   ret; })

#define _exit(status) \
  ({ asm volatile("syscall" : : "a"(60), "D"(status)); __builtin_unreachable(); })


asm(".type _start, @function\n"
    ".global _start\n"
    "_start:\n"
    "jmp code\n"
    ".size _start, .-_start");


void code() { _exit(write(1, "hi mom\n", 7)); }


/*
$ gcc bug.c -nostartfiles -static
$ ./a.out
hi mom
$ gcc bug.c -nostartfiles -static -O3          # or any -O
$ ./a.out
hi mom
$ gcc bug.c -nostartfiles -static -flto
$ ./a.out
hi mom
$ gcc bug.c -nostartfiles -static -O3 -flto
/tmp/ccVQVnd9.ltrans0.ltrans.o: In function `_start':
<artificial>:(.text+0x5): undefined reference to `__start'
collect2: error: ld returned 1 exit status


$ clang bug.c -nostartfiles -static
$ ./a.out
hi mom
$ clang bug.c -nostartfiles -static -flto
/tmp/lto-llvm-57f30a.o: In function `_start':
ld-temp.o:(.text+0x5): undefined reference to `__start'
clang-3.9: error: linker command failed with exit code 1 (use -v to see invocation)
*/
